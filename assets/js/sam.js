const { spawn } = nodeRequire("child_process");
const { shell } = nodeRequire('electron');
const fs = nodeRequire('fs');
const ini = nodeRequire('ini');

$( document ).ready(function() {
  // Update UI
  $('#avd-loading').css('display', 'inherit');
  
  $('#btn-sdk').click(() => {
    openAndroidSDK();
  });
  $('#btn-avd').click(() => {
    openAvdFolder();
  });
  $('#btn-add-avd').click(() => {
    addAvd();
  })
  
  // Run program
  listAvds((avds) => {
    $('#avd-loading').hide();
    if (avds.length > 0) {
      $('#avd-fetched').css('display', 'inherit');
      showAvdList(avds);
    } else {
      $('#avd-none').css('display', 'inherit');
    }
  }, (error) => {
    console.error(error);
  });
});

function showAvdInfo(avdName) {
  $('.list-group-item').removeClass('active');
  $('#avd-'+avdName).addClass('active');
  $('#avd-list-pane').addClass('with-info-right');
  $('#avd-create-pane').hide();
  $('#avd-info-pane').show();
  
  // Read avd info
  var avdConfig = ini.parse(fs.readFileSync(process.env.HOME + '/.android/avd/' + avdName + '.avd/config.ini', 'utf-8'));
  var avdHardware = ini.parse(fs.readFileSync(process.env.HOME + '/.android/avd/' + avdName + '.avd/hardware-qemu.ini', 'utf-8'));
  
  $('#info-avd-name').val(avdName);
  $('#info-avd-manu').val(avdConfig['hw.device.manufacturer']);
  $('#info-avd-device').val(avdConfig['hw.device.name']);
  $('#info-avd-archi').val(avdConfig['hw.cpu.arch']);
  $('#info-avd-playstore').val(avdConfig['PlayStore.enabled']);
  $('#info-avd-ram').val(avdHardware['hw.ramSize'] + ' MB');
  $('#info-avd-size').val((parseFloat(avdConfig['disk.dataPartition.size']) / 1073741824) + ' GB');
  $('#info-avd-accelerometer').text(avdConfig['hw.accelerometer']);
  $('#info-avd-gps').text(avdConfig['hw.gps']);
  $('#info-avd-battery').text(avdConfig['hw.battery']);
  $('#info-avd-screen').val(avdConfig['hw.lcd.width'] + 'w x ' + avdConfig['hw.lcd.height'] + 'h');
  $('#info-avd-density').val(avdConfig['hw.lcd.density'] + ' dpi');
}

function listAvds(onAvds, onError) {
  const ANDROID_HOME = process.env.ANDROID_HOME;
  const avdListRunnable = spawn(ANDROID_HOME + "/emulator/emulator", ["-list-avds" ]);
  avdListRunnable.stdout.on("data", data => {
    onAvds(data.toString().split('\n').filter(e => e.length > 0));
  });
  avdListRunnable.stderr.on("data", error => {
    onError(error.toString());
  });
}

function addAvd() {
  $('.list-group-item').removeClass('active');
  $('#avd-list-pane').addClass('with-info-right');
  $('#avd-info-pane').hide();
  $('#avd-create-pane').show();
  
  
}

function startAvd(avdName) {
  const ANDROID_HOME = process.env.ANDROID_HOME;
  const avdStartRunnable = spawn(ANDROID_HOME + "/emulator/emulator", ["-avd", avdName ]);
  avdStartRunnable.stdout.on("data", data => {
    console.log('Avd started', data.toString());
  });
  avdStartRunnable.stderr.on("data", error => {
    console.error('Avd start error', error.toString());
  });
}

function deleteAvd(avdName) {
  const avdDeleteRunnable = spawn("rm", ["-rf", "~/.android/avd/"+ avdName +".avd", "~/.android/avd/"+ avdName +".ini"]);
  avdDeleteRunnable.stdout.on("data", data => {
    console.log('Avd deleted', data.toString());
  });
  avdDeleteRunnable.stderr.on("data", error => {
    console.error('Avd delete error', error.toString());
  });
}

function openAvdFolder() {
  shell.openPath(process.env.HOME + '/.android/avd');
}

function openAndroidSDK() {
  shell.openPath(process.env.ANDROID_HOME);
}

function showAvdList(avds) {
  const avdsListHtml = [];
  avds.forEach((avdName) => {
    var avdHtml = $('<li class="list-group-item" id="avd-'+avdName+'"></li>');
    var avdHtmlBody = $('<div class="media-body"><strong>'+avdName.replace(/_/g, ' ')+'</strong></div>');
    
    var avdHtmlActions = $('<div class="pull-right"></div>');
    var avdHtmlStart = $('<button class="btn btn-default"><span class="icon icon-play"></span></button>');
    
    avdHtmlStart.click(() => {
      startAvd(avdName);
    });
    avdHtmlActions.append(avdHtmlStart);
    avdHtmlBody.append(avdHtmlActions);
    
    avdHtml.html(avdHtmlBody);
    avdHtml.click(() => {
      showAvdInfo(avdName);
    });
    
    avdsListHtml.push(avdHtml);
  });
  
  $('#avd-list').html(avdsListHtml);
  showAvdInfo(avds[0]);
}